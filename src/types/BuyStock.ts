type BuyStock = {
  id?: number
  name: string
  balance: number
  unit: string
  cost: number
}
export type { BuyStock }
