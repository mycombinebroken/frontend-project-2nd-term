import type { Stock } from '@/types/Stock'
import http from './http'

function addStock(stock: Stock & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', stock.name)
  formData.append('status', stock.status)
  formData.append('min', stock.min.toString())
  formData.append('balance', stock.balance.toString())
  formData.append('unit', stock.unit)
  formData.append('branch', JSON.stringify(stock.branch))
  if (stock.files && stock.files.length > 0) formData.append('file', stock.files[0])
  return http.post('/stocks', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateStock(stock: Stock & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', stock.name)
  formData.append('status', stock.status)
  formData.append('balance', stock.balance.toString())
  formData.append('min', stock.min.toString())
  formData.append('unit', stock.unit)
  console.log(JSON.stringify(stock.branch))
  formData.append('branch', JSON.stringify(stock.branch))
  if (stock.files && stock.files.length > 0) formData.append('file', stock.files[0])
  return http.post(`/stocks/${stock.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delStock(stock: Stock) {
  return http.delete(`/stocks/${stock.id}`)
}

function getStock(id: number) {
  return http.get(`/stocks/${id}`)
}

function getStocks() {
  return http.get('/stocks')
}

export default { addStock, updateStock, delStock, getStock, getStocks }
